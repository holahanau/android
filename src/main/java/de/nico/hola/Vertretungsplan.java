package de.nico.hola;

/* 
 * Author: Nico Alt
 * See the file "LICENSE.txt" for the full license governing this code.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

import de.nico.asura.R;
import de.nico.asura.tools.Utils;

public class Vertretungsplan extends Activity {

    // Where to download the representation plan
    private static final String downURL = "http://gymnasium-hola.de/images/stories/vertretungen1/android-app-verification.php?";
    private static final String PREFS = "HoLaVertretungen";
    // Place of representation plan
    private static final File file = new File(
            Environment.getExternalStorageDirectory()
                    + "/HoLa/Vertretungsplan.pdf");
    private static final Uri dest = Uri.parse("file:"
            + Environment.getExternalStorageDirectory()
            + "/HoLa/Vertretungsplan.pdf");
    // First time?
    private static String first;
    // Password for representation plan
    private static String pass;
    // ID of download by DownloadManager
    private static long downID;
    private final BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(downID);
            Cursor cursor = downManager.query(query);

            if (cursor.moveToFirst()) {
                int columnIndex = cursor
                        .getColumnIndex(DownloadManager.COLUMN_STATUS);
                int status = cursor.getInt(columnIndex);

                // Success?
                if (status == DownloadManager.STATUS_SUCCESSFUL) {
                    update();
                    open(null);

                } else if (status == DownloadManager.STATUS_FAILED) {
                    Utils.makeLongToast(Vertretungsplan.this,
                            getString(R.string.down_error));

                } else if (status == DownloadManager.STATUS_PAUSED) {
                    Utils.makeLongToast(Vertretungsplan.this,
                            getString(R.string.down_paused));

                } else if (status == DownloadManager.STATUS_PENDING) {
                    Utils.makeLongToast(Vertretungsplan.this,
                            getString(R.string.down_pending));

                } else if (status == DownloadManager.STATUS_RUNNING) {
                    Utils.makeLongToast(Vertretungsplan.this,
                            getString(R.string.down_running));

                }

            }

        }

    };
    // DownloadManager
    private static DownloadManager downManager;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getShared();

        downManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

        // Check if it's first start or if there is no password
        if (!(first.equals("1")) || pass.equals("0")) {
            setContentView(R.layout.hola_check);
            return;

        }

        goHome();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            //noinspection ConstantConditions
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, intentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(downloadReceiver);

    }

    private void getShared() {
        // Get strings from SharedPreferences
        SharedPreferences holashared = getSharedPreferences(PREFS, 0);
        first = holashared.getString("first", "0");
        pass = holashared.getString("pass", "0");
    }

    private void goHome() {
        setContentView(R.layout.hola_vertretung);
        update();

    }

    private void update() {
        TextView text_last_modified = (TextView) findViewById(R.id.textView_lastmodified);

        // If file does not exist show never downloaded
        if (!(file.exists())) {
            text_last_modified.setText("Noch nie");
            findViewById(R.id.button_open).setVisibility(View.INVISIBLE);
            return;

        }

        // Get date when representation plan was last modified
        Date datelastModified = new Date(file.lastModified());

        // DateFormat is DD.MM.YY (e.g. 01.01.1970)
        DateFormat dateFormat = android.text.format.DateFormat
                .getDateFormat(this);

        // Set date when last downloaded
        text_last_modified.setText(String.valueOf(dateFormat
                .format(datelastModified)));

    }

    public void download(View view) {
        Utils.makeShortToast(this, getString(R.string.toast_wait));

        if (file.exists())
            //noinspection ResultOfMethodCallIgnored
            file.delete();

        // Download representation plan
        Request request = new Request(Uri.parse(downURL + "down=" + pass));
        request.setTitle("Vertretungsplan").setDestinationUri(dest);
        downID = downManager.enqueue(request);

    }

    public void open(View view) {
        if (file.exists()) {
            try {
                Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
                pdfIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(pdfIntent);

            } catch (ActivityNotFoundException e) {
                Utils.makeLongToast(this, getString(R.string.except_nopdf));

            }

        }

    }

    public void check(View view) {
        Utils.makeLongToast(this, getString(R.string.toast_wait));

        // Crashes when this is not set
        ThreadPolicy policy = new ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        EditText Feld = (EditText) findViewById(R.id.edit_check);
        final String wrong_answer = getString(R.string.wrong);

        // Close keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(Feld.getWindowToken(), 0);

        // Check if something is filled in or if it is too long
        if (Feld.getText().toString().length() == 0
                || Feld.getText().toString().length() > 20) {
            Utils.makeLongToast(this, wrong_answer);
            return;

        }

        String password = Feld.getText().toString();

        HTMLPageDownloader hpd = new HTMLPageDownloader(downURL + "verify="
                + password);
        String check = hpd.doInBackground();

        if (!(check.equals("true"))) {
            Utils.makeLongToast(this, wrong_answer);
            return;
        }

        SharedPreferences prefs = getSharedPreferences(PREFS, 0);
        Editor editor = prefs.edit();
        editor.putString("first", "1");
        editor.putString("pass", password);
        editor.apply();

        pass = password;
        goHome();

    }

}