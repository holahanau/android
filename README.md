# HoLa-Android

[![Play Store](https://developer.android.com/images/brand/en_generic_rgb_wo_60.png)](https://play.google.com/store/apps/details?id=de.nico.holavertretungen)

## Mitarbeiten

Wenn du beim Inhalt der App helfen willst, kannst du auf der
[Webseite](http://hola-gymnasium.de/) nach neuen PDF-Dateien suchen und
mich per [E-Mail](mailto:nicoalt@posteo.org) darüber informieren oder
einen [Issue](https://gitlab.com/holahanau/data/issues) auf GitLab
öffnen.

## Wofür ist dieses Projekt?

Dieses Projekt enthält den Quellcode der offiziellen Android App der
Hohen Landesschule in Hanau.

## Wie fange ich an?

Importiere das Projekt in Android Studio und kompiliere es, um eine
`.apk` Datei zu erhalten, welche du auf einem Android Gerät installieren
kannst.

## TODO

- Android: [Issues](https://gitlab.com/holahanau/android/issues)
- Data: [Issues](https://gitlab.com/holahanau/data/issues)
- Upstream: [Issues](https://gitlab.com/asura/android/issues)

## Lizenz

Siehe
[LICENSE](./LICENSE).
